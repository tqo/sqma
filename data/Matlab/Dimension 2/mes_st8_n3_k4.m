E=@(n)exp(2*1i*pi/n);
sq=sqrtm([[ 96, 0 ]; [ 0, 96 ]; ]);
d=2;
k=4;
n=3;
mes=zeros(2,2,3,4);
mes(:,:,1,1)=sq*[[ 1/3, 1/3*E(8)^3 ]; [ -1/3*E(8), 1/3 ]; ]*sq^-1;
mes(:,:,2,1)=sq*[[ 1/3-1/6*E(8)+1/6*E(8)^3, 1/6*E(8)-1/6*E(8)^3 ]; [ 1/6*E(8)-1/6*E(8)^3, 1/3+1/6*E(8)-1/6*E(8)^3 ]; ]*sq^-1;
mes(:,:,3,1)=sq*[[ 1/3+1/6*E(8)-1/6*E(8)^3, -1/6*E(8)-1/6*E(8)^3 ]; [ 1/6*E(8)+1/6*E(8)^3, 1/3-1/6*E(8)+1/6*E(8)^3 ]; ]*sq^-1;
mes(:,:,1,2)=sq*[[ 1/3, 1/3*E(8) ]; [ -1/3*E(8)^3, 1/3 ]; ]*sq^-1;
mes(:,:,2,2)=sq*[[ 1/3+1/6*E(8)-1/6*E(8)^3, -1/6*E(8)+1/6*E(8)^3 ]; [ -1/6*E(8)+1/6*E(8)^3, 1/3-1/6*E(8)+1/6*E(8)^3 ]; ]*sq^-1;
mes(:,:,3,2)=sq*[[ 1/3-1/6*E(8)+1/6*E(8)^3, -1/6*E(8)-1/6*E(8)^3 ]; [ 1/6*E(8)+1/6*E(8)^3, 1/3+1/6*E(8)-1/6*E(8)^3 ]; ]*sq^-1;
mes(:,:,1,3)=sq*[[ 1/3, -1/3*E(8)^3 ]; [ 1/3*E(8), 1/3 ]; ]*sq^-1;
mes(:,:,2,3)=sq*[[ 1/3+1/6*E(8)-1/6*E(8)^3, 1/6*E(8)+1/6*E(8)^3 ]; [ -1/6*E(8)-1/6*E(8)^3, 1/3-1/6*E(8)+1/6*E(8)^3 ]; ]*sq^-1;
mes(:,:,3,3)=sq*[[ 1/3-1/6*E(8)+1/6*E(8)^3, -1/6*E(8)+1/6*E(8)^3 ]; [ -1/6*E(8)+1/6*E(8)^3, 1/3+1/6*E(8)-1/6*E(8)^3 ]; ]*sq^-1;
mes(:,:,1,4)=sq*[[ 1/3+1/6*E(8)-1/6*E(8)^3, 1/6*E(8)-1/6*E(8)^3 ]; [ 1/6*E(8)-1/6*E(8)^3, 1/3-1/6*E(8)+1/6*E(8)^3 ]; ]*sq^-1;
mes(:,:,2,4)=sq*[[ 1/3, -1/3*E(8) ]; [ 1/3*E(8)^3, 1/3 ]; ]*sq^-1;
mes(:,:,3,4)=sq*[[ 1/3-1/6*E(8)+1/6*E(8)^3, 1/6*E(8)+1/6*E(8)^3 ]; [ -1/6*E(8)-1/6*E(8)^3, 1/3+1/6*E(8)-1/6*E(8)^3 ]; ]*sq^-1;
